package ru.t1.fpavlov.tm.enumerated;

import java.util.Arrays;

/**
 * Created by fpavlov on 25.11.2021.
 */
public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String displayValue) {
        if (displayValue == null || displayValue.isEmpty()) return null;
        for (final Status item : values()) {
            if (item.displayName.equals(displayValue)) return item;
        }
        return null;
    }

    public static String[] displayValues() {
        final Status statusNames[] = Status.values();
        final String statusValues[] = new String[statusNames.length];
        for (int i = 0; i < statusValues.length; i++) {
            statusValues[i] = statusNames[i].getDisplayName();
        }
        return statusValues;
    }

}
