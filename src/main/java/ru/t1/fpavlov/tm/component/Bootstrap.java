package ru.t1.fpavlov.tm.component;

import ru.t1.fpavlov.tm.api.controller.ICommandController;
import ru.t1.fpavlov.tm.api.controller.IProjectController;
import ru.t1.fpavlov.tm.api.controller.IProjectTaskController;
import ru.t1.fpavlov.tm.api.controller.ITaskController;
import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.*;
import ru.t1.fpavlov.tm.controller.*;
import ru.t1.fpavlov.tm.exception.system.ArgumentIncorrectException;
import ru.t1.fpavlov.tm.exception.system.CommandIncorrectException;
import ru.t1.fpavlov.tm.repository.*;
import ru.t1.fpavlov.tm.service.*;
import ru.t1.fpavlov.tm.util.TerminalUtil;


import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository,
            taskRepository
    );

    private final IProjectTaskController projectTaskController = new ProjectTaskController(
            projectTaskService
    );

    private final IProjectController projectController = new ProjectController(
            projectService,
            projectTaskService
    );

    private final ILoggerService loggerService = new LoggerService();

    private void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case CMD_PROJECT_LIST:
                projectController.showAll();
                break;
            case CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case CMD_PROJECT_FIND_BY_ID:
                projectController.showById();
                break;
            case CMD_PROJECT_FIND_BY_INDEX:
                projectController.showByIndex();
                break;
            case CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case CMD_PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case CMD_PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case CMD_TASK_CREATE:
                taskController.create();
                break;
            case CMD_TASK_LIST:
                taskController.showAll();
                break;
            case CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case CMD_TASK_FIND_BY_ID:
                taskController.showById();
                break;
            case CMD_TASK_FIND_BY_INDEX:
                taskController.showByIndex();
                break;
            case CMD_TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case CMD_TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case CMD_TASK_START_BY_ID:
                taskController.startById();
                break;
            case CMD_TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case CMD_TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case CMD_TASK_FIND_ALL_BY_PROJECT_ID:
                taskController.showAllByProjectId();
                break;
            case CMD_BIND_TASK_TO_PROJECT_BY_ID:
                projectTaskController.bindTaskToProject();
                break;
            case CMD_UNBIND_TASK_TO_PROJECT_BY_ID:
                projectTaskController.unbindTaskToProject();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                throw new CommandIncorrectException();
        }
    }

    private void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                commandController.displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_SHORT_VERSION:
                commandController.displayVersion();
                break;
            case CMD_SHORT_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_SHORT_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_SHORT_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_SHORT_PROJECT_CREATE:
                projectController.create();
                break;
            case CMD_SHORT_PROJECT_LIST:
                projectController.showAll();
                break;
            case CMD_SHORT_PROJECT_CLEAR:
                projectController.clear();
                break;
            case CMD_SHORT_TASK_CREATE:
                taskController.create();
                break;
            case CMD_SHORT_TASK_LIST:
                taskController.showAll();
                break;
            case CMD_SHORT_TASK_CLEAR:
                taskController.clear();
                break;
            default:
                throw new ArgumentIncorrectException();
        }

        System.exit(0);
    }

    private void initLogger() {
        this.loggerService.info("**WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("TASK-MANAGER IS SHUTTING DOWN");
            }
        });
    }

    public void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            try {
                listenerArgument(args[0]);
                this.loggerService.command(args[0]);
                return;
            } catch (Exception e) {
                this.loggerService.error(e);
                return;
            }
        }

        interactiveCommandProcessing();
    }

    private void interactiveCommandProcessing() {
        this.InitDemoData();
        this.initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("-- Please enter a command --");
                final String param = TerminalUtil.nextLine();
                listenerCommand(param);
                System.out.println("Ok");
                this.loggerService.command(param);
            } catch (Exception e) {
                this.loggerService.error(e);
            }
        }
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void quite() {
        System.exit(0);
    }

    private void InitDemoData() {
        this.projectService.create("Test project 1", "Test project 1");
        this.projectService.create("Test project 2", "Test project 2");
        this.projectService.create("Test project 3", "Test project 3");
        this.projectService.create("Test project 4", "Test project 4");
        this.projectService.create("Test project 5", "Test project 5");

        this.taskService.create("Test task 1", "Test task 1");
        this.taskService.create("Test task 2", "Test task 2");
        this.taskService.create("Test task 3", "Test task 3");
        this.taskService.create("Test task 4", "Test task 4");
        this.taskService.create("Test task 5", "Test task 5");
        this.taskService.create("Test task 6", "Test task 6");
        this.taskService.create("Test task 7", "Test task 7");
        this.taskService.create("Test task 8", "Test task 8");
        this.taskService.create("Test task 9", "Test task 9");
        this.taskService.create("Test task 10", "Test task 10");
        this.taskService.create("Test task 11", "Test task 11");
        this.taskService.create("Test task 12", "Test task 12");
        this.taskService.create("Test task 13", "Test task 13");
        this.taskService.create("Test task 14", "Test task 14");
        this.taskService.create("Test task 15", "Test task 15");
    }
}
