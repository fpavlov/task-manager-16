package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.controller.ITaskController;
import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void create() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        this.taskService.create(name, description);
    }

    @Override
    public void clear() {
        System.out.println("Clear task");
        taskService.clear();
    }

    @Override
    public void showAll() {
        System.out.println("Available sort:");
        System.out.println(Arrays.toString(Sort.displayValues()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> items = taskService.findAll(sort);
        renderTasks(items);
    }

    @Override
    public void showAllByProjectId() {
        System.out.println("Enter the project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Available sort:");
        System.out.println(Arrays.toString(Sort.displayValues()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> items = this.taskService.findAllByProjectId(projectId);
        renderTasks(items);
    }

    @Override
    public void removeById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        this.taskService.removeById(itemId);
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        this.taskService.removeByIndex(itemIndex - 1);
    }

    @Override
    public void showById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        final Task item = this.taskService.findById(itemId);
        System.out.println(item);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final Task item = this.taskService.findByIndex(itemIndex - 1);
        System.out.println(item);
    }

    @Override
    public void updateById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        this.taskService.updateById(itemId, name, description);
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        System.out.println("Enter new Name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description");
        final String description = TerminalUtil.nextLine();
        this.taskService.updateByIndex(itemIndex - 1, name, description);
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        this.taskService.changeStatusById(itemId, status);
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        System.out.println("Available statuses:");
        System.out.println(Arrays.toString(Status.displayValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        this.taskService.changeStatusByIndex(itemIndex - 1, status);
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        this.taskService.changeStatusById(itemId, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        this.taskService.changeStatusByIndex(itemIndex - 1, Status.IN_PROGRESS);
    }

    @Override
    public void completeById() {
        System.out.println("Enter id");
        final String itemId = TerminalUtil.nextLine();
        this.taskService.changeStatusById(itemId, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() {
        System.out.println("Enter index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        this.taskService.changeStatusByIndex(itemIndex - 1, Status.COMPLETED);
    }

    public void renderTasks(final List<Task> items) {
        int index = 1;
        System.out.println("Projects list:");
        for (final Task item : items) {
            if (item == null) continue;
            System.out.format("|%2d%s%n", index, item);
            index++;
        }
        System.out.println("\tOk");
    }

}
