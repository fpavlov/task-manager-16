package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.controller.ICommandController;
import ru.t1.fpavlov.tm.api.service.ICommandService;
import ru.t1.fpavlov.tm.model.Command;

import static ru.t1.fpavlov.tm.util.FormatUtil.bytesToHumanReadable;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argumentName = command.getArgument();
            if (argumentName == null || argumentName.isEmpty()) {
                continue;
            }
            System.out.println(argumentName);
        }
    }

    @Override
    public void displayCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String commandName = command.getName();
            if (commandName == null || commandName.isEmpty()) {
                continue;
            }
            System.out.println(commandName);
        }
    }

    @Override
    public void displayWelcomeText() {
        System.out.println("**WELCOME TO TASK MANAGER**");
    }

    @Override
    public void displayHelp() {
        final Command[] commands = commandService.getTerminalCommands();
        System.out.println("Commands:");
        for (final Command command : commands) {
            System.out.println("\t" + command);
        }
    }

    @Override
    public void displayAbout() {
        System.out.println("\tPavlov Philipp");
        System.out.println("\tfpavlov@t1-consulting.ru");
    }

    @Override
    public void displayVersion() {
        System.out.println("\t1.16.0");
    }

    @Override
    public void incorrectCommand() {
        System.out.println("\tIncorrect Command");
    }

    @Override
    public void incorrectArgument() {
        System.out.println("\tIncorrect Argument");
    }

    @Override
    public void displaySystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        System.out.format(
                "System info:%n" +
                        "\t - Available processors: %s%n" +
                        "\t - Free memory: %s%n" +
                        "\t - Maximum memory: %s%n" +
                        "\t - Total memory: %s%n" +
                        "\t - Used memory: %s%n",
                runtime.availableProcessors(),
                bytesToHumanReadable(runtime.freeMemory()),
                bytesToHumanReadable(runtime.maxMemory()),
                bytesToHumanReadable(runtime.totalMemory()),
                bytesToHumanReadable(runtime.totalMemory() - runtime.freeMemory())
        );
    }

}
